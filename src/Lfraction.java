import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
	   //Lfraction esimene = new Lfraction(2, 5);
	   //Lfraction teine = new Lfraction(4,10);
	   //teine.reduce();
	   //System.out.println(esimene.equals(teine));
	   //Lfraction kolmas = valueOf(" /2");
	   //System.out.println(esimene.equals(2));
   }

   // TODO!!! instance variables here

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   private long lugeja;
   private long nimetaja;
   
   public Lfraction (long a, long b) {
	   if(b==0) {
		   throw new RuntimeException("nimetaja on 0");
	   }
	   else if(b<0) {
		   //System.out.println("korrutasin -1 a"+);
		   this.lugeja = -a;
		   this.nimetaja = -b;
	   }
	   else {
		   this.lugeja = a;
		   this.nimetaja = b;
	   }
	   
   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return this.lugeja; // TODO!!!
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return this.nimetaja; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return ""+this.lugeja+"/"+this.nimetaja;
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
	   Lfraction esimene =this.reduce();
	   Lfraction teine;
	   
	   try {
	   teine = ((Lfraction) m).reduce();
	   }
	   catch(Exception classCastException) {
		   throw new RuntimeException("object "+m+" ei ole Lfraction t��pi");
	   }
	   
	   if(esimene.getNumerator()==teine.getNumerator() && esimene.getDenominator() == teine.getDenominator()) {
		   return true;
	   }
	   return false; // TODO!!!
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
	   int hashCode = (int)this.lugeja * 300 + (int)this.nimetaja * 5;
	   return hashCode;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
	   Lfraction esimene=this.reduce();
	   Lfraction teine=m.reduce();
	   long uuslugeja=esimene.getNumerator()*teine.getDenominator()+ teine.getNumerator()*esimene.getDenominator();
	   long uusnimetaja=esimene.getDenominator()*teine.getDenominator();
	   System.out.println("vahepeatus "+uuslugeja+" / "+uusnimetaja);
	   Lfraction vastus = new Lfraction(uuslugeja, uusnimetaja);
	   vastus=vastus.reduce();
	   
      return vastus; // TODO!!!
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
	   Lfraction esimene=this.reduce();
	   Lfraction teine=m.reduce();
	   long uuslugeja=esimene.getNumerator() * teine.getNumerator();
	   long uusnimetaja=esimene.getDenominator()*teine.getDenominator();
	   Lfraction vastus = new Lfraction(uuslugeja, uusnimetaja);
	   vastus=vastus.reduce();
      return vastus; // TODO!!!
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      return new Lfraction(this.nimetaja, this.lugeja);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(this.lugeja*-1,this.nimetaja);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
	   Lfraction esimene=this.reduce();
	   Lfraction teine=m.reduce();
	   long uuslugeja=esimene.getNumerator()*teine.getDenominator()- teine.getNumerator()*esimene.getDenominator();
	   long uusnimetaja=esimene.getDenominator()*teine.getDenominator();
	   Lfraction vastus = new Lfraction(uuslugeja, uusnimetaja);
	   vastus=vastus.reduce();
      return vastus;
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
	   Lfraction esimene=this.reduce();
	   Lfraction teine=m.reduce();
	   long uuslugeja=esimene.getNumerator() * teine.getDenominator();
	   long uusnimetaja=esimene.getDenominator()*teine.getNumerator();
	   Lfraction vastus = new Lfraction(uuslugeja, uusnimetaja);
	   vastus=vastus.reduce();
      return vastus; // TODO!!!
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
	   if(this.equals(m)) {
		   return 0;
	   }
	   float esimene = (float)this.lugeja/(float)this.nimetaja;
	   //System.out.println("esimene "+esimene);
	   float teine = (float)m.getNumerator()/(float)m.getDenominator();
	   //System.out.println("teine "+teine);
	   if(esimene > teine) {
		   return 1;
	   }
	   else {
		   return -1;
	   }
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.lugeja, this.nimetaja);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.lugeja / this.nimetaja; // TODO!!!
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
	   long uuslugeja= this.lugeja - this.integerPart()*this.nimetaja;
	   
      return new Lfraction(uuslugeja, this.nimetaja);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.lugeja / (double)this.nimetaja;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
	   //System.out.println(f*(double)d);
	   //System.out.println(Math.round(f*(double) d));
	   long uusLugeja = (Math.round(f*(double) d));
      return new Lfraction(uusLugeja, d); // TODO!!!
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
	   String[] osad = s.split("/");
	   try {
       long uusLugeja = Long.parseLong(osad[0]);
       long uusNimetaja = Long.parseLong(osad[1]);
       return new Lfraction(uusLugeja, uusNimetaja);
	   }
	   catch(Exception parseException){
		   throw new RuntimeException("String "+s+" on loetamatu");
	   }
   }
   
   public Lfraction reduce() {
	   long muutuja = GCD (this.lugeja, this.nimetaja);
	   return new Lfraction(this.lugeja/muutuja, this.nimetaja/muutuja);
   }
   // Kasutasin koodi stackoverflowest https://stackoverflow.com/questions/4009198/java-get-greatest-common-divisor
   //koodi omanik on Matt https://stackoverflow.com/users/447191/matt
   public long GCD(long a, long b) {
	   if (b==0) return a;
	   return GCD(b,a%b);
	}
}

